/*
	Copyright (c) 2012 Cycling '74

	Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
	and associated documentation files (the "Software"), to deal in the Software without restriction, 
	including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
	and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
	subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all copies 
	or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
	IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
	OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

package list;
import com.cycling74.max.*;

	public void copyFrom(String frombuf, long destoffset) {
		copyFrom(frombuf, destoffset, 0);
	}
	public void copyFrom(String frombuf, long destoffset, long srcoffset) {
		long length= MSPBuffer.getSize(frombuf);
		copyFrom(frombuf, destoffset, srcoffset,  length-srcoffset);
	}
	public void copyFrom(final String frombuf, final long destoffset, final long srcoffset, final long srcsize) {
		MaxSystem.deferLow(new Executable() {	
			public void execute() {	
				int channels= MSPBuffer.getChannels(frombuf);
				for(int i= 1; i<=channels; i++) {
					MSPBuffer.poke(bufname, i, destoffset, MSPBuffer.peek(frombuf, i, srcoffset, srcsize));
				}
				outlet(0, "done");
			}
		});
	}

