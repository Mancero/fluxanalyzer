FluxAnalyzer_1.0 Readme.txt

Dispositif d'analyse musicale fondé sur la description acoustique bas-niveau, la segmentation automatisée et la décomposition harmonique des flux audio.

Développé par Daniel Mancero, CICM - Université Paris 8 — ©2019

www.danielmancero.com
